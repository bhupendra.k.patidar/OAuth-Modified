package com.intient.internal;

public class ApiResponse<T> {

	private String status;
	private int statusCode;
	private String message;
	private T body;

	public enum STATUS {
		SUCCESS, FAILURE;
	}

	public ApiResponse(String status, int statusCode, String message, T body) {
		this.status = status;
		this.statusCode = statusCode;
		this.message = message;
		this.body = body;
	}

	public ApiResponse(int statusCode, String message, T body) {
		if (statusCode == 200) {
			this.status = "SUCCESS";
		} else {
			this.status = "FAILURE";
		}
		this.statusCode = statusCode;
		this.message = message;
		this.body = body;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public T getBody() {
		return body;
	}

	public void setBody(T body) {
		this.body = body;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + body.hashCode();
		result = prime * result + message.hashCode();
		result = prime * result + status.hashCode();
		result = prime * result + statusCode;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ApiResponse other = (ApiResponse) obj;
		if (!equalsCheck(other)) {
			return false;
		}
		return true;
	}

	private Boolean equalsCheck(ApiResponse other) {
		if (body == null) {
			if (other.body != null)
				return false;
		} else if (!body.equals(other.body))
			return false;
		if (message == null) {
			if (other.message != null)
				return false;
		} else if (!message.equals(other.message))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		if (statusCode != other.statusCode)
			return false;
		return true;
	}

}
