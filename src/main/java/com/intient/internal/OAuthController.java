package com.intient.internal;

import java.io.IOException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;

import com.accenture.intient.studio.bq.dto.BigQueryErrorDTO;
import com.accenture.intient.studio.bq.dto.BigQueryExecuteQueryDTO;
import com.accenture.intient.studio.bq.dto.ExecuteQueryDataDTO;
import com.accenture.intient.studio.bq.dto.FieldsDTO;
import com.accenture.intient.studio.bq.dto.GetRowsSchemaDTO;
import com.accenture.intient.studio.bq.dto.JobReferenceDTO;
import com.accenture.intient.studio.bq.dto.SchemaDTO;
import com.fasterxml.jackson.databind.node.ObjectNode;
//import com.accenture.intient.studio.dto.ApiResponse;
//import com.accenture.intient.studio.exception.StudioException;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeRequestUrl;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.auth.oauth2.UserCredentials;
import com.google.cloud.bigquery.BigQuery;
import com.google.cloud.bigquery.BigQueryException;
import com.google.cloud.bigquery.BigQueryOptions;
import com.google.cloud.bigquery.Dataset;
import com.google.cloud.bigquery.Field;
import com.google.cloud.bigquery.FieldValueList;
import com.google.cloud.bigquery.Job;
import com.google.cloud.bigquery.JobException;
import com.google.cloud.bigquery.JobInfo;
import com.google.cloud.bigquery.JobStatistics;
import com.google.cloud.bigquery.QueryJobConfiguration;
import com.google.cloud.bigquery.TableResult;
import com.google.cloud.bigquery.BigQuery.QueryResultsOption;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.intient.internal.BigQueryDatasets;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.ResponseBody;

@Controller
public class OAuthController {
	
	private static final List<String> scope = Arrays.asList("https://www.googleapis.com/auth/userinfo.email", "https://www.googleapis.com/auth/userinfo.profile",
            "https://www.googleapis.com/auth/compute.readonly", "https://www.googleapis.com/auth/cloud-platform");
    private static final java.io.File credentialFile = new java.io.File(System.getenv("HOME"),
            "GoogleOAuth2Credentials");
    public static String CALLBACK_URL;
    private static final String CLIENT_ID = System.getenv("CLIENT_ID");
    private static final String CLIENT_SECRET = System.getenv("CLIENT_SECRET");
    private static FileDataStoreFactory SampleFileDataStoreFactory;
    private static HttpTransport HTTP_TRANSPORT;
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
	
	
	
	static {
        CALLBACK_URL = System.getenv("CALLBACK_URL");

        try {
            HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
            SampleFileDataStoreFactory = new FileDataStoreFactory(credentialFile);
        } catch (Throwable t) {
            t.printStackTrace();
            System.exit(1);
        }
    }
	
	/**
     * Authorizes the Java application to access user's protected data.
     */
    private static GoogleAuthorizationCodeFlow authorize() throws IOException {
        // set up authorization code flow
        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
                HTTP_TRANSPORT, JSON_FACTORY,
                CLIENT_ID,
                CLIENT_SECRET,
                scope)
                .setDataStoreFactory(SampleFileDataStoreFactory)
                .setAccessType("offline")
                //.setApprovalPrompt("auto") 
                .setApprovalPrompt("force") // need to force to get the refresh token
                .build();
        return flow;
    }
    
    @GetMapping("/login")
    public RedirectView login(HttpSession session) throws IOException {
        GoogleAuthorizationCodeFlow flow = authorize();
        String email = (String) session.getAttribute("userid");
        if(email != null) {
        	String authCode = (String)session.getAttribute("auth");      	
        	return new RedirectView("/callback?code="+authCode);
        } else {

            String stateToken = getStateToken();
            session.setAttribute("state", stateToken);

            String url = buildLoginUrl(flow, stateToken);
            return new RedirectView(url); // https://www.baeldung.com/spring-redirect-and-forward
        
        }
    }
	
	
	@GetMapping("/callback")
	public String callback3(HttpSession session, @RequestParam(value = "code") String authCode) throws Exception {
		session.removeAttribute("state");
	    GoogleAuthorizationCodeFlow flow = authorize();
	    
	    String email = (String) session.getAttribute("userid");
	    Credential credential = null;
	    
	    if(email == null) {
	    	//GoogleClie
	    	GoogleTokenResponse tokenResponse = flow.newTokenRequest(authCode).setRedirectUri(CALLBACK_URL).execute();
	    	email = getUserEmail(tokenResponse);
	    	credential = flow.createAndStoreCredential(tokenResponse, email);
	        String refreshToken = tokenResponse.getRefreshToken();
	        //tokenResponse.ge
	        session.setAttribute("refreshToken", refreshToken);
	    	session.setAttribute("userid", email);
	        session.setAttribute("auth", authCode);
	    } else {
	    	
	    	email = (String)session.getAttribute("userid");
	    	credential = flow.loadCredential(email);
	    }    
	    
		return "greeting";
	}
	
	
    
    
	
	



	public String buildLoginUrl(GoogleAuthorizationCodeFlow flow, String stateToken) {
	    final GoogleAuthorizationCodeRequestUrl url = flow.newAuthorizationUrl();
	    return url.setRedirectUri(CALLBACK_URL).setState(stateToken).build();
	}
	
	/**
	 * Generates a secure state token
	 */
	public String getStateToken() {
	    SecureRandom sr1 = new SecureRandom();
	    return "google;" + sr1.nextInt();
	}
	
	/**
	 * Expects an Authentication Code, and makes an authenticated request for the user's profile information
	 * * @param authCode authentication code provided by google
	 */
	@GetMapping("/callbackxxx")
	@org.springframework.web.bind.annotation.ResponseBody
	public BigQueryDatasets callback(HttpSession session, @RequestParam(value = "code") String authCode) throws Exception {
	    session.removeAttribute("state");
	    GoogleAuthorizationCodeFlow flow = authorize();
	    
	    String email = (String) session.getAttribute("userid");
	    Credential credential = null;
	    
	    if(email == null) {
	    	//GoogleClie
	    	GoogleTokenResponse tokenResponse = flow.newTokenRequest(authCode).setRedirectUri(CALLBACK_URL).execute();
	    	email = getUserEmail(tokenResponse);
	    	credential = flow.createAndStoreCredential(tokenResponse, email);
	        String refreshToken = tokenResponse.getRefreshToken();
	        //tokenResponse.ge
	        session.setAttribute("refreshToken", refreshToken);
	    	session.setAttribute("userid", email);
	        session.setAttribute("auth", authCode);
	    } else {
	    	
	    	email = (String)session.getAttribute("userid");
	    	credential = flow.loadCredential(email);
	    }           
	    
	    
	    GoogleCredentials gCredentials = UserCredentials.newBuilder()
	            .setClientId(CLIENT_ID)
	            .setClientSecret(CLIENT_SECRET)
	            .setRefreshToken(credential.getRefreshToken())
	            .build();
	
	    List<String> datasets = getDatasets(gCredentials, "bigquery-public-data");
	   
	
	    BigQueryDatasets bigQueryDatasets = new BigQueryDatasets(
	            credential.getAccessToken(),
	            credential.getRefreshToken(),
	            email,
	            datasets);
	
	    return bigQueryDatasets;
	}

	public List<String> getDatasets(GoogleCredentials gCredentials, String projectId) throws IOException {
	
	
	    BigQuery bigquery = BigQueryOptions.newBuilder()
	            .setCredentials(gCredentials)
	            .setProjectId(projectId)
	            .build()
	            .getService();
	    List<String> datasets = new ArrayList<>();
	    for (Dataset dataset : bigquery.listDatasets().iterateAll()) {
	        datasets.add(dataset.getDatasetId().getDataset());
	    }
	
	    return datasets;
	}
	
	private String getUserEmail(GoogleTokenResponse response) throws IOException {
	    Request request = new Request.Builder()
	            .url("https://www.googleapis.com/oauth2/v1/userinfo?access_token=" + response.getAccessToken())
	            .build();
	    OkHttpClient client = new OkHttpClient();
	    ResponseBody responseBody = client.newCall(request).execute().body();
	    JsonObject userinfo = JsonParser.parseString(responseBody.string()).getAsJsonObject();
	    String email = userinfo.get("email").getAsString();
	    return email;
	}
	
	@GetMapping("/projects/{projectId}/datasets")
	@org.springframework.web.bind.annotation.ResponseBody
	public ResponseEntity<ServiceResponse> getDatasets(HttpSession session, @PathVariable String projectId) throws IOException {
	    GoogleAuthorizationCodeFlow flow = authorize();
	    String email = (String) session.getAttribute("userid");
	
	    Credential credential = flow.loadCredential(email);
	    //credential.getRefreshToken();
	    
	    String refreshToken = (String)session.getAttribute("refreshToken");
	
	    GoogleCredentials gCredentials = UserCredentials.newBuilder()
	            .setClientId(CLIENT_ID)
	            .setClientSecret(CLIENT_SECRET)
	            .setRefreshToken(refreshToken) // cr
	            .build();
	    List<String> datasets = getDatasets(gCredentials, projectId);
	    BigQueryDatasets bigQueryDatasets = new BigQueryDatasets(
	            credential.getAccessToken(),
	            refreshToken,
	            email,
	            datasets);
	    
	    ServiceResponse<BigQueryDatasets> serviceRes = new ServiceResponse<BigQueryDatasets>("success", bigQueryDatasets);
	
	    return new ResponseEntity<ServiceResponse>(serviceRes, HttpStatus.OK);
	}
	
	
	//@ApiOperation(value = "execute query", notes = "This activity execute the query as per user request.")
	@PostMapping("/big-query-client/execute-query")
	public ResponseEntity<ApiResponse<BigQueryExecuteQueryDTO>> getQueryResults(HttpSession session, @RequestBody ObjectNode json) {

		String projectId = json.get("projectId").asText();
		String query = json.get("query").asText();
		ApiResponse<BigQueryExecuteQueryDTO> response = null;
		BigQueryExecuteQueryDTO res = new BigQueryExecuteQueryDTO();
		try {
			res = executeQuery(session, projectId, query);
			response = new ApiResponse("SUCCESS", 200, "Query executed successfully", res);
			return ResponseEntity.ok().body(response);
		} catch (Exception ex) {
			//BigQueryErrorDTO error = new BigQueryErrorDTO()
			response = new ApiResponse<>("FAILED", 400, "Error executing execute query", null);
			return ResponseEntity.status(400).body(response);
		}
	}
	
	/*
	 * Executes the query and returns results
	 */
	
	public BigQueryExecuteQueryDTO executeQuery(HttpSession session, String projectId, String query) throws IOException {
		
		GoogleAuthorizationCodeFlow flow = authorize();
	    String email = (String) session.getAttribute("userid");
	    String refreshToken = (String)session.getAttribute("refreshToken");
	    Credential credential = flow.loadCredential(email);
	    
	    String accessToken = credential.getAccessToken();
	    Calendar c = Calendar.getInstance();
	    c.add(Calendar.MINUTE, 10);
	    Date date = c.getTime();
	    AccessToken at = new AccessToken(accessToken, date);
	    GoogleCredentials gCredentials = UserCredentials.newBuilder()
	            .setClientId(CLIENT_ID)
	            .setClientSecret(CLIENT_SECRET).setAccessToken(at)
	            //.setRefreshToken(refreshToken)
	            .build();
	    

	    
	    BigQuery bigQuery = BigQueryOptions.newBuilder().setCredentials(gCredentials).setProjectId(projectId).build().getService();
		//BigQuery bigquery = bigQueryServiceImplUtil.getBigQueryClient(projectId, credentials);

		BigQueryExecuteQueryDTO executeQueryOutput = new BigQueryExecuteQueryDTO();
		ExecuteQueryDataDTO executeQueryDataDTO = new ExecuteQueryDataDTO();
		JobReferenceDTO jobReferenceDTO = new JobReferenceDTO();
		SchemaDTO schemaDTO = new SchemaDTO();
		try {
			QueryJobConfiguration queryJobConfiguration = QueryJobConfiguration.newBuilder(query)
					.setUseQueryCache(false).setDryRun(false).build();
			JobInfo jobInfo = JobInfo.of(queryJobConfiguration);
			Job job = bigQuery.create(jobInfo);
			job = job.waitFor();
			jobReferenceDTO.setJobId(job.getJobId().getJob());
			jobReferenceDTO.setProjectId(job.getJobId().getProject());
			jobReferenceDTO.setLocation(job.getJobId().getLocation());
			executeQueryDataDTO.setJobReference(jobReferenceDTO);
			executeQueryDataDTO.setJobComplete(job.getStatus().getState().name());
			JobStatistics.QueryStatistics statistics = job.getStatistics();
			executeQueryDataDTO.setCacheHit(statistics.getCacheHit().toString());
			executeQueryDataDTO.setTotalBytesProcessed(statistics.getTotalBytesProcessed().toString());
			TableResult executeQueryResult = job.getQueryResults(QueryResultsOption.pageSize(20));
			executeQueryDataDTO.setTotalRows(String.valueOf(executeQueryResult.getTotalRows()));
			GetRowsSchemaDTO getRowsSchemaDTO = getRowsSchemaList(executeQueryResult);
			schemaDTO.setFields(getRowsSchemaDTO.getFieldsList());
			executeQueryDataDTO.setSchema(schemaDTO);
			executeQueryDataDTO.setRows(getRowsSchemaDTO.getRowList());
			executeQueryOutput.setData(executeQueryDataDTO);
		} catch (BigQueryException ex) {
			String errorMessage = ex.getMessage() + "#" + ex.getCode();
			System.out.println(errorMessage);
		} catch (JobException | InterruptedException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		return executeQueryOutput;
	}
	
	public GetRowsSchemaDTO getRowsSchemaList(TableResult executeQueryResult) {
		GetRowsSchemaDTO getRowsSchemaDTO = new GetRowsSchemaDTO();
		List<FieldsDTO> fieldsList = new ArrayList<>();
		List<Map<String, String>> rowList = new ArrayList<>();
		Iterator<Field> fieldIterator = executeQueryResult.getSchema().getFields().iterator();
		List<String> colNames = new ArrayList<>();
		while (fieldIterator.hasNext()) {
			FieldsDTO fieldsDTO = new FieldsDTO();
			Field fieldNext = fieldIterator.next();
			String colName = fieldNext.getName();

			fieldsDTO.setMode(fieldNext.getMode().name());
			fieldsDTO.setName(colName);
			fieldsDTO.setType(fieldNext.getType().name());

			fieldsList.add(fieldsDTO);
			colNames.add(colName);
		}
		Iterator<FieldValueList> valuesIterator = executeQueryResult.getValues().iterator();
		while (valuesIterator.hasNext()) {
			FieldValueList row = valuesIterator.next();
			Map<String, String> rowMap = new HashMap<>();
			for (int i = 0; i < colNames.size(); i++) {
				String rowVal = row.get(colNames.get(i)).getStringValue();
				rowMap.put(colNames.get(i), rowVal);
			}
			rowList.add(rowMap);
		}
		getRowsSchemaDTO.setFieldsList(fieldsList);
		getRowsSchemaDTO.setRowList(rowList);

		return getRowsSchemaDTO;
	}
	
	
	
}
