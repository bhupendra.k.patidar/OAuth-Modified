package com.accenture.intient.studio.bq.dto;

import java.util.List;

public class BigQuerySearchProjectsDTO {

	private String projectId;
	
	private List<BigQuerySearchDatasetDTO> datasets;


	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public List<BigQuerySearchDatasetDTO> getDatasets() {
		return datasets;
	}

	public void setDatasets(List<BigQuerySearchDatasetDTO> datasets) {
		this.datasets = datasets;
	}

	
	
}
