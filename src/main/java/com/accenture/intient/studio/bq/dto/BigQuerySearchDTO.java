package com.accenture.intient.studio.bq.dto;

public class BigQuerySearchDTO {

	private BigQuerySearchDataDTO data;

	public BigQuerySearchDataDTO getData() {
		return data;
	}

	public void setData(BigQuerySearchDataDTO data) {
		this.data = data;
	}
	
	
}
