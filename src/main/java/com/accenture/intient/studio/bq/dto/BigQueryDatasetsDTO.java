package com.accenture.intient.studio.bq.dto;

import java.util.List;

public class BigQueryDatasetsDTO {

	private String projectId;
	private List<DatasetIdDTO> datasets;

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public List<DatasetIdDTO> getDatasets() {
		return datasets;
	}

	public void setDatasets(List<DatasetIdDTO> datasets) {
		this.datasets = datasets;
	}

}
