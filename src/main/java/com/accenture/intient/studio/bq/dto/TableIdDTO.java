package com.accenture.intient.studio.bq.dto;

public class TableIdDTO {

	private String tableId;

	public String getTableId() {
		return tableId;
	}

	public void setTableId(String tableId) {
		this.tableId = tableId;
	}

}
