package com.accenture.intient.studio.bq.dto;

public class ProjectIdDTO {

	private String projectId;

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
}
