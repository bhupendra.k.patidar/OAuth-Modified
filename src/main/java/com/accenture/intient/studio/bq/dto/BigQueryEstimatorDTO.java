package com.accenture.intient.studio.bq.dto;

public class BigQueryEstimatorDTO {
	
	private String totalBytesProcessed;

	public String getTotalBytesProcessed() {
		return totalBytesProcessed;
	}

	public void setTotalBytesProcessed(String totalBytesProcessed) {
		this.totalBytesProcessed = totalBytesProcessed;
	}

}
