package com.accenture.intient.studio.bq.dto;

import java.util.List;

public class BigQueryProjectsDTO {

	private List<ProjectIdDTO> projects;

	public List<ProjectIdDTO> getProjects() {
		return projects;
	}

	public void setProjects(List<ProjectIdDTO> projects) {
		this.projects = projects;
	}

}
