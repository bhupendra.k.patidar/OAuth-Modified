package com.accenture.intient.studio.bq.dto;

import java.util.List;

public class BigQuerySearchRowsDTO {
	
	private List<BigQuerySearchProjectsDTO> projects;

	public List<BigQuerySearchProjectsDTO> getProjects() {
		return projects;
	}

	public void setProjects(List<BigQuerySearchProjectsDTO> projects) {
		this.projects = projects;
	}
	
	
}
