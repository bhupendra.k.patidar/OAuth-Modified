package com.accenture.intient.studio.bq.dto;

import java.util.List;
import java.util.Map;

public class ExecuteQueryDataDTO {

	private SchemaDTO schema;
	private List<Map<String, String>> rows;
	private String kind;
	private String cacheHit;
	private String jobComplete;
	private String totalRows;
	private JobReferenceDTO jobReference;
	private String totalBytesProcessed;

	public SchemaDTO getSchema() {
		return schema;
	}

	public void setSchema(SchemaDTO schema) {
		this.schema = schema;
	}

	public List<Map<String, String>> getRows() {
		return rows;
	}

	public void setRows(List<Map<String, String>> rows) {
		this.rows = rows;
	}

	public String getKind() {
		return kind;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}

	public String getCacheHit() {
		return cacheHit;
	}

	public void setCacheHit(String cacheHit) {
		this.cacheHit = cacheHit;
	}

	public String getJobComplete() {
		return jobComplete;
	}

	public void setJobComplete(String jobComplete) {
		this.jobComplete = jobComplete;
	}

	public String getTotalRows() {
		return totalRows;
	}

	public void setTotalRows(String totalRows) {
		this.totalRows = totalRows;
	}

	public JobReferenceDTO getJobReference() {
		return jobReference;
	}

	public void setJobReference(JobReferenceDTO jobReference) {
		this.jobReference = jobReference;
	}

	public String getTotalBytesProcessed() {
		return totalBytesProcessed;
	}

	public void setTotalBytesProcessed(String totalBytesProcessed) {
		this.totalBytesProcessed = totalBytesProcessed;
	}
}
