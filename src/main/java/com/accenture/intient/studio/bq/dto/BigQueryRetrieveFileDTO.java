package com.accenture.intient.studio.bq.dto;

public class BigQueryRetrieveFileDTO {
	
	private String name;
	private String timecreated;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTimecreated() {
		return timecreated;
	}
	public void setTimecreated(String timecreated) {
		this.timecreated = timecreated;
	}
}
