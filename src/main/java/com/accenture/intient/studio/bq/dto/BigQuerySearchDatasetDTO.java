package com.accenture.intient.studio.bq.dto;

import java.util.List;

public class BigQuerySearchDatasetDTO {
	
	private String datasetId;
	
	private List<TableIdDTO> tables;

	public String getDatasetId() {
		return datasetId;
	}

	public void setDatasetId(String datasetId) {
		this.datasetId = datasetId;
	}

	public List<TableIdDTO> getTables() {
		return tables;
	}

	public void setTables(List<TableIdDTO> tables) {
		this.tables = tables;
	}
	
	
}
