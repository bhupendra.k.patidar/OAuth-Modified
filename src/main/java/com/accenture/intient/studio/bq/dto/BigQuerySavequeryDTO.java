package com.accenture.intient.studio.bq.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BigQuerySavequeryDTO {
	@JsonProperty("query")
	private String query;

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	@Override
	public String toString() {
		return "BigQuerySavequeryDTO [query=" + query + "]";
	}

}