package com.accenture.intient.studio.bq.dto;

public class DatasetIdDTO {

	private String datasetId;

	public String getDatasetId() {
		return datasetId;
	}

	public void setDatasetId(String datasetId) {
		this.datasetId = datasetId;
	}
}
