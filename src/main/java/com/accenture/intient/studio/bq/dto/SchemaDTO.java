package com.accenture.intient.studio.bq.dto;

import java.util.List;

public class SchemaDTO {

	private List<FieldsDTO> fields;

	public List<FieldsDTO> getFields() {
		return fields;
	}

	public void setFields(List<FieldsDTO> fields) {
		this.fields = fields;
	}
}
