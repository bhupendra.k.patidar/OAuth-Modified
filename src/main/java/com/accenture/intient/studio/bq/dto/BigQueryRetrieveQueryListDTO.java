package com.accenture.intient.studio.bq.dto;

import java.util.List;

public class BigQueryRetrieveQueryListDTO {
	public List<BigQueryRetrieveFileDTO> getItems() {
		return items;
	}

	public void setItems(List<BigQueryRetrieveFileDTO> items) {
		this.items = items;
	}

	private List<BigQueryRetrieveFileDTO> items;
}
