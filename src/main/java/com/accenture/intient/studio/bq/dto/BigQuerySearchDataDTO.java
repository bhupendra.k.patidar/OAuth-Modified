package com.accenture.intient.studio.bq.dto;

public class BigQuerySearchDataDTO {
	
	private BigQuerySearchTextDTO rows;

	public BigQuerySearchTextDTO getRows() {
		return rows;
	}

	public void setRows(BigQuerySearchTextDTO rows) {
		this.rows = rows;
	}
	
	
}
