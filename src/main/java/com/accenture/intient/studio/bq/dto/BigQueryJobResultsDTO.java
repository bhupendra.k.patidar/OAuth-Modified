package com.accenture.intient.studio.bq.dto;

public class BigQueryJobResultsDTO {

	private ExecuteQueryDataDTO data;

	public ExecuteQueryDataDTO getData() {
		return data;
	}

	public void setData(ExecuteQueryDataDTO data) {
		this.data = data;
	}
	
}
