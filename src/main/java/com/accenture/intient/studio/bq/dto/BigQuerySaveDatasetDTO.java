package com.accenture.intient.studio.bq.dto;

public class BigQuerySaveDatasetDTO {
	private String datasetId;
	
	private String tableId;

	public String getTableId() {
		return tableId;
	}

	public void setTableId(String tableId) {
		this.tableId = tableId;
	}

	public String getDatasetId() {
		return datasetId;
	}

	public void setDatasetId(String datasetId) {
		this.datasetId = datasetId;
	}
}
