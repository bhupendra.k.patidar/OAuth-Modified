package com.accenture.intient.studio.bq.dto;

public class ViewIdDTO {
	
	private String viewId;

	public String getViewId() {
		return viewId;
	}

	public void setViewId(String viewId) {
		this.viewId = viewId;
	}
	
}
