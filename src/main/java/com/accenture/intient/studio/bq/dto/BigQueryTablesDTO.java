package com.accenture.intient.studio.bq.dto;

import java.util.List;

public class BigQueryTablesDTO {

	
	private String projectId;
	
	
	private String datasetId;
	
	
	private List<TableIdDTO> tables ;
	
	private List<ViewIdDTO> views;
	
	public List<ViewIdDTO> getViews() {
		return views;
	}


	public void setViews(List<ViewIdDTO> views) {
		this.views = views;
	}


	public List<TableIdDTO> getTables() {
		return tables;
	}


	public void setTables(List<TableIdDTO> tables) {
		this.tables = tables;
	}


	public String getProjectId() {
		return projectId;
	}


	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}


	public String getDatasetId() {
		return datasetId;
	}


	public void setDatasetId(String datasetId) {
		this.datasetId = datasetId;
	}

}
