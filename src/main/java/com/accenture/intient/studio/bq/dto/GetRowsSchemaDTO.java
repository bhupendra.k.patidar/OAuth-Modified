package com.accenture.intient.studio.bq.dto;

import java.util.List;
import java.util.Map;

public class GetRowsSchemaDTO {

	List<FieldsDTO> fieldsList;

	List<Map<String, String>> rowList;

	public List<FieldsDTO> getFieldsList() {
		return fieldsList;
	}

	public void setFieldsList(List<FieldsDTO> fieldsList) {
		this.fieldsList = fieldsList;
	}

	public List<Map<String, String>> getRowList() {
		return rowList;
	}

	public void setRowList(List<Map<String, String>> rowList) {
		this.rowList = rowList;
	}

}
